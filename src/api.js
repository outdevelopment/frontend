import 'axios';
import Axios from 'axios';

const host = '35.232.124.123'
const port = '8000'

let ElasticAPI = Axios.create({
	baseURL: `http://${host}:${port}/elastic/`
})

let handleError = (error) => {
	console.log(error)
	window.alert("Error occured, for more details check the console")
}

ElasticAPI.getUser = async function(username=""){
	let {data} = await this.get(`/user/${username}`).catch(error => handleError(error))
	return data
}

ElasticAPI.putUser = async function(username="", body={}){
	let {data} = await this.put(`/user/${username}`, body).catch(error => handleError(error))
	return data
}

ElasticAPI.deleteUser = async function(username){
	let {data} = await this.delete(`/user/${username}`).catch(error => handleError(error))
	return data
}

ElasticAPI.enableUser = async function(username){
	let {data} = await this.put(`/user/${username}/enable`).catch(error => handleError(error))
	return data
}

ElasticAPI.disableUser = async function(username){
	let {data} = await this.put(`/user/${username}/disable`).catch(error => handleError(error))
	return data
}

export {
	ElasticAPI
}