import React from 'react';
import { Input, Form, Button } from 'antd'
import { ElasticAPI } from '../api'

const USER_FORM_FIELDS = [
	{required:true, key:"username", defaultValue:"", label:"Username", type:"text"},
	{required:true, key:"password", defaultValue:"", label:"Password", type:"password", minLength:"6"},
	{required:false, key:"email", defaultValue:"", label:"Email", type:"email"},
	{required:false, key:"full_name", defaultValue:"", label:"Full Name", type:"text"},
]

class CreateUser extends React.Component {
	state = {
		user: {}
	}

	async handleSubmit(event){
		event.preventDefault()
		let { user } = this.state;

		// Check if username is taked
		if((await ElasticAPI.getUser(user.username))[0]){
			alert("Username is taken, please enter a new one")
			user.username = ""
			this.setState({user})
		} else {
			await ElasticAPI.putUser(user.username, user)
			this.props.history.push('/users')
		}
	}

	handleInput(event, key){
		let { user } = this.state;
		user[key] = event.target.value;
		this.setState({user})
	}

	render() {
		return (
			<Form onSubmit={this.handleSubmit.bind(this)} >
				{
					USER_FORM_FIELDS.map(field => {
						return (
							<Form.Item {...field}>
								<Input 
									{...field} 
									value={this.state.user[field.key]} 
									onChange={(e) => this.handleInput(e, field.key)}/>	
							</Form.Item>
						)
					})
				}
				<Button type="primary" htmlType="submit">Create User</Button>
			</Form>
		);
	}
}

export default CreateUser;
