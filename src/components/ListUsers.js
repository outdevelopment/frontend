import React from 'react';
import { Input, List, Icon, Tooltip } from 'antd'
import { ElasticAPI } from '../api'
import { Link } from 'react-router-dom'

const CreateUserIcon = () => (
	<div style={{ textAlign: "center", margin: "3em" }}>
		<Link to="/users/new">
			<Tooltip title="Create New User">
				<Icon style={{ fontSize: '3em' }} type="user-add" />
			</Tooltip>
		</Link>
	</div>
)

function sort_by_key(list, key){
	return list.sort((a,b) => a[key] > b[key])
}

function query_objects(objects, query, fields){
	if(query){
		return objects.filter(obj => {
			let corpus = String()
			for (var field of fields){
				if(obj[field])
					corpus += String(obj[field])
			}
			return corpus.includes(query)
	
		})
	} else {
		return objects;
	}
	
}

class ListUsers extends React.Component {
	constructor() {
		super();
		this.state = {
			users: [],
			search: null
		}

		this.setSearch = this.setSearch.bind(this);
	}

	componentDidMount() {
		this.fetchUsers()
	}

	setSearch(value) {
		this.setState({ search: value });
	}
	async fetchUsers() {
		let users = await ElasticAPI.getUser();
		this.setState({ users });
	}
	async deleteUser(username) {
		let confirmation = window.confirm("Are you sure?")
		if (confirmation && username) {
			await ElasticAPI.deleteUser(username)
			this.fetchUsers()
		}
	}

	render() {
		let users = this.state.users || [];
		
		// Filter By Search
		users = query_objects(users, this.state.search, ["username", "email", "full_name"])

		// Sort alphabetically
		users = sort_by_key(users)

		return (
			<div>
				<Input.Search
					value={this.state.search}
					size="large"
					placeholder="Search Users"
					onInputCapture={(e) => this.setSearch(e.target.value)} />

				<List size="large" mode="vertical" itemLayout="horizontal">
					{
						users.map(user => {
							let reserved = user.metadata._reserved;
							let actions = []

							if (reserved) {
								actions.push(<Link to={`users/${user.username}`}>view</Link>)
							} else {
								actions.push(<Link to={`users/${user.username}`}>edit</Link>)
								actions.push(<Link onClick={() => this.deleteUser(user.username)}>delete</Link>)
							}

							return (
								<List.Item actions={actions} key={user.username}>
									<List.Item.Meta
										title={user.username}
										description={(user.email || "")}
										avatar={""}/>

								</List.Item>
							)
						})
					}
				</List>

				<CreateUserIcon />
			</div>
		);
	}
}

export default ListUsers;
