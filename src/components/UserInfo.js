import React from 'react'
import { ElasticAPI } from '../api';
import { Typography, Switch } from 'antd'

const Title = Typography.Title
const Text = Typography.Text

const USER_FIELDS = [
	{key:"enabled", type:"bool", label:"Enabled"},
	{key:"full_name", type:"text", label:"Full Name"},
	{key:"email", type:"text", label:"Email"},
]

let UserInfoField = ({type="text", fieldKey, fieldValue, label, change, editable=false}) => {
	// use the key as the default label
	if(!label)
		label = fieldKey

	// default value
	let value = fieldValue

	switch (type) {
		case "bool":
			value = <Switch disabled={!editable} onChange={(value) => change(type, fieldKey, value)} size="small" checked={fieldValue}/>
			break;
		default:
			// default value for default type
			
			value = (
				<Text
					copyable 
					editable={
						editable ? { onChange: (value) => change(type, fieldKey, value)}:false
					}>
					{fieldValue || ""}
				</Text>
			)
	}

	return (
		<div style={{margin:"1.5em 0"}}>
			<Text strong>{label}: </Text>
			{value}
		</div>
	)
}

class UserInfo extends React.Component{
	state = {
		user: {},
		is_editable: false
	}

	componentWillMount(){
		const {username} = this.props.match.params;
		this.setState({username})
	}

	componentDidMount(){
		this.fetchUser()
	}

	async fetchUser(){
		const { username } = this.state;
		if(username){
			let user = (await ElasticAPI.getUser(username))[0]
			let is_editable = true
			if(user.metadata)
				if(user.metadata._reserved)
					is_editable = false
			this.setState({user, is_editable})
		}
	}

	async editUserField(type, key, value){
		let { user } = this.state
		user[key] = value
		console.log("Sending user: ", user)
		await ElasticAPI.putUser(this.state.username, user)
		this.fetchUser()
	}

	render(){
		let { user, username } = this.state

		return (
			<div>
				<Title level={1}>{username} {!this.state.is_editable ? "[reserved]" : ""}</Title>
				{
					USER_FIELDS.map(field => {
						return (
							<UserInfoField
								key={field.key}
								label={field.label}
								editable={this.state.is_editable}
								type={field.type} 
								change={this.editUserField.bind(this)} 
								fieldKey={field.key} 
								fieldValue={user[field.key]}/>
						)
					})
				}
			</div>
		)
	}
}

export default UserInfo