import React from 'react'
import { Link } from 'react-router-dom'
import { Typography, Button } from 'antd'
import ElasticImage from '../static/elastic.png'

const Welcome = (props) => {
	return (
		<div style={{textAlign:"center"}}>
			<img  src={ElasticImage} alt="elastic" height="450px"/>
			<Typography.Title level={1}>
				Elastic User Management
			</Typography.Title>
			<Link to="/users">
				<Button type="primary">
					Manage Users
				</Button>
			</Link>
		</div>
	)
}

export default Welcome