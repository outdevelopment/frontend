import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { Menu, Icon, Layout } from 'antd'

import Welcome from './components/Welcome'
import ListUsers from './components/ListUsers'
import UserInfo from "./components/UserInfo";
import CreateUser from "./components/CreateUser";


const MENU_ITEMS = [
	{ key: "1", display: "Home", url: "/", icon: "home"},
	{ key: "2", display: "Users", url: "/users", icon: "user" },
]

const NavBar = () => {
	return (
		<Menu theme="dark">
			{MENU_ITEMS.map(item => {
				return (
					<Menu.Item mode="inline" key={item.url} >
						<Link to={item.url}>
							{item.icon ? <Icon type={item.icon}/> : "" }
							<span>{item.display}</span>
						</Link>
					</Menu.Item>
				)
			})}
		</Menu>
	)
}

class AppRouter extends React.Component{
	state = {
		sider_collapsed: false
	}

	toggleSider = (collapsed) => this.setState({sider_collapsed: collapsed})
	
	render(){
		return (
			<Router>
				<Layout style={{minHeight: '100vh', width:"100vw"}} hasSider={true}>
					<Layout.Sider 
						collapsible 
						collapsed={this.state.sider_collapsed}
						onCollapse={this.toggleSider}>

						<NavBar/>
					</Layout.Sider>
					<div style={{padding: "6em", width:"100%"}}>
						<Route path="/" exact component={Welcome} />
						<Route exact path="/users/" component={ListUsers} />
						<Switch>
							<Route path="/users/new" component={CreateUser} />
							<Route path="/users/:username" component={UserInfo} />
						</Switch>
					</div>
				</Layout>
			</Router>
		  );
	}
}

export default AppRouter;
